# LibreOffice 6.4.1.2 de 32 bit


LibreOffice 6.4.1.2 de 32 bit DEB compilado por Escuelas Linux, para Debian, Ubuntu, Linux Mint, y otros Linux Basados en Ubuntu o Debian de 32 bits. Las instrucciones están en:<br />

[https://facilitarelsoftwarelibre.blogspot.com/2020/04/libreoffice-para-linux-de-32-bit.html](https://facilitarelsoftwarelibre.blogspot.com/2020/04/libreoffice-para-linux-de-32-bit.html) 

En **Escuelas Linux 6.8** de 32 bit LibreOffice 6.4.1.2 ya viene pre-instalado:<br />
[https://sourceforge.net/projects/escuelaslinux/files/](https://sourceforge.net/projects/escuelaslinux/files/) <br />

<div data-sourcepos="8:1-8:84" dir="auto">
<h3>
<b>Funciona también en los sabores de 32 bit de Ubuntu 18.04 </b></h3>
como lo son:</div>

<div data-sourcepos="10:1-11:119" dir="auto">
Lubuntu 18.04.4 LTS (Bionic Beaver)
&nbsp;</div>
<div data-sourcepos="10:1-11:119" dir="auto">
<a href="http://cdimage.ubuntu.com/lubuntu/releases/18.04/release/">http://cdimage.ubuntu.com/lubuntu/releases/18.04/release/</a></div>
<div data-sourcepos="10:1-11:119" dir="auto">
Buscar todas las liberaciones:</div>
<div data-sourcepos="10:1-11:119" dir="auto">
<a href="http://cdimage.ubuntu.com/lubuntu/releases/">http://cdimage.ubuntu.com/lubuntu/releases/</a></div>
<div data-sourcepos="13:1-14:123" dir="auto">
Kubuntu 18.04.4 LTS (Bionic Beaver)&nbsp;</div>
<div data-sourcepos="13:1-14:123" dir="auto">
<a href="http://cdimage.ubuntu.com/kubuntu/releases/18.04.4/release/">http://cdimage.ubuntu.com/kubuntu/releases/18.04.4/release/</a></div>
<div data-sourcepos="13:1-14:123" dir="auto">
Buscar todas las liberaciones:</div>
<div data-sourcepos="13:1-14:123" dir="auto">
<a href="http://cdimage.ubuntu.com/kubuntu/releases/">http://cdimage.ubuntu.com/kubuntu/releases/</a></div>
<div data-sourcepos="16:1-17:135" dir="auto">
Ubuntu Budgie 18.04.4 LTS (Bionic Beaver)
&nbsp;</div>
<div data-sourcepos="16:1-17:135" dir="auto">
<a href="http://cdimage.ubuntu.com/ubuntu-budgie/releases/18.04.4/release/">http://cdimage.ubuntu.com/ubuntu-budgie/releases/18.04.4/release/</a></div>
<div data-sourcepos="16:1-17:135" dir="auto">
Buscar todas las liberaciones:</div>
<div data-sourcepos="16:1-17:135" dir="auto">
<a href="http://cdimage.ubuntu.com/ubuntu-budgie/releases/">http://cdimage.ubuntu.com/ubuntu-budgie/releases/</a></div>
<div data-sourcepos="19:1-20:131" dir="auto">
Ubuntu MATE 18.04.4 LTS (Bionic Beaver)
&nbsp;</div>
<div data-sourcepos="19:1-20:131" dir="auto">
<a href="http://cdimage.ubuntu.com/ubuntu-mate/releases/18.04.4/release/">http://cdimage.ubuntu.com/ubuntu-mate/releases/18.04.4/release/</a></div>
<div data-sourcepos="19:1-20:131" dir="auto">
Buscar todas las liberaciones:</div>
<div data-sourcepos="19:1-20:131" dir="auto">
<a href="http://cdimage.ubuntu.com/ubuntu-mate/releases/">http://cdimage.ubuntu.com/ubuntu-mate/releases/</a></div>
<div data-sourcepos="22:1-23:119" dir="auto">
Xubuntu 18.04.4 LTS (Bionic Beaver)
&nbsp;</div>
<div data-sourcepos="22:1-23:119" dir="auto">
<a href="http://cdimage.ubuntu.com/xubuntu/releases/18.04/release/">http://cdimage.ubuntu.com/xubuntu/releases/18.04/release/</a></div>
<div data-sourcepos="22:1-23:119" dir="auto">
Buscar todas las liberaciones:</div>
<div data-sourcepos="22:1-23:119" dir="auto">
<a href="http://cdimage.ubuntu.com/xubuntu/releases/">http://cdimage.ubuntu.com/xubuntu/releases/</a></div>
<div data-sourcepos="22:1-23:119" dir="auto">
Ubuntu Kylin 18.04.4 LTS (Bionic Beaver)</div>
<div data-sourcepos="22:1-23:119" dir="auto">
<a href="http://cdimage.ubuntu.com/ubuntukylin/releases/18.04.4/release/">http://cdimage.ubuntu.com/ubuntukylin/releases/18.04.4/release/</a>&nbsp;</div>
<div data-sourcepos="22:1-23:119" dir="auto">
Buscar todas las liberaciones:</div>
<div data-sourcepos="22:1-23:119" dir="auto">
<a href="http://cdimage.ubuntu.com/ubuntukylin/releases/">http://cdimage.ubuntu.com/ubuntukylin/releases/</a></div>
<div data-sourcepos="22:1-23:119" dir="auto">
Ubuntu Studio 18.04 LTS (Bionic Beaver)</div>
<div data-sourcepos="22:1-23:119" dir="auto">
<a href="http://cdimage.ubuntu.com/ubuntustudio/releases/18.04/release/">http://cdimage.ubuntu.com/ubuntustudio/releases/18.04/release/</a></div>
<div data-sourcepos="22:1-23:119" dir="auto">
Buscar todas las liberaciones:</div>
<div data-sourcepos="22:1-23:119" dir="auto">
<a href="http://cdimage.ubuntu.com/ubuntustudio/releases/">http://cdimage.ubuntu.com/ubuntustudio/releases/</a></div>
<b>Nota: </b>Algunas de esas distribuciones dejaron de dar soporte hace 
mucho tiempo para 32 bit, como ejemplo UbuntuStudio 18.04, para más 
información lea <a href="https://facilitarelsoftwarelibre.blogspot.com/2020/05/sabores-ubuntu-1604-1804-de-32-bit.html" target="_blank">esta entrada</a>.<br />
<br />

<div data-sourcepos="25:1-25:29" dir="auto">
<h3>
<b>También funciona en Debian 10 basados en</b></h3>
</div>
<div data-sourcepos="27:1-27:60" dir="auto">
Debian 10 Buster y Sistemas Operativos de 32 bit basados en este como:</div>
<div data-sourcepos="29:1-31:79" dir="auto">
antiX 19
&nbsp;</div>
<div data-sourcepos="29:1-31:79" dir="auto">
<a href="https://antixlinux.com/antix-19-isos-available/" rel="nofollow noreferrer noopener" target="_blank">https://antixlinux.com/antix-19-isos-available/</a>
<a href="https://antixlinux.com/torrent-files/" rel="nofollow noreferrer noopener" target="_blank">https://antixlinux.com/torrent-files/</a></div>
<div data-sourcepos="33:1-34:75" dir="auto">
MX Linux 19
&nbsp;</div>
<div data-sourcepos="33:1-34:75" dir="auto">
<a href="https://mxlinux.org/download-links/" rel="nofollow noreferrer noopener" target="_blank">https://mxlinux.org/download-links/</a></div>
<br />

<h3>
<b>Debería funcionarn Linux Mint basados en Ubuntu Bionic 18.04 de 32 bits</b></h3>
Para que vea cuales son las versiones entre en:<br />
<a href="https://linuxmint.com/download.php">https://linuxmint.com/download.php</a><br />
<a href="https://1.bp.blogspot.com/-E6Klb57xA_w/Xrgh-ZZ1uFI/AAAAAAAAbo4/Ib1ceKfgjqgeLNeE5DXNQvPCVQra1uljgCK4BGAYYCw/s1600/20200510-103539%2BLinux%2BMint%2Bbasados%2Ben%2BUbuntu%2BBionic%2B18.04.png"><img border="0" src="https://1.bp.blogspot.com/-E6Klb57xA_w/Xrgh-ZZ1uFI/AAAAAAAAbo4/Ib1ceKfgjqgeLNeE5DXNQvPCVQra1uljgCK4BGAYYCw/s1600/20200510-103539%2BLinux%2BMint%2Bbasados%2Ben%2BUbuntu%2BBionic%2B18.04.png" /></a><br />

sólo en esas versiones funcionaría LibreOffice 6.4 compilado por 
Escuelas Linux (Todavía no he probado pero debería funcionar bien allí).<br />



**Código Fuente**<br />
Pueden ver el código fuente usado aquí:<br />
[https://downloadarchive.documentfoundation.org/libreoffice/old/6.4.1.2/](https://downloadarchive.documentfoundation.org/libreoffice/old/6.4.1.2/) 


